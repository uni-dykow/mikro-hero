namespace MikroHero
{
    public sealed class PlayerFemale : APlayer
    {
        public PlayerFemale()
        {
            Greeting();
        }

        protected override void Greeting()
        {
            System.Console.WriteLine("Witaj wojowniczko!");
        }
    }
}