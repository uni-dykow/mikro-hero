﻿namespace MikroHero
{
   public interface  IWeaponStrategy
    {
        int Dmg  { get; set; }
        public int Use();
    }
}
