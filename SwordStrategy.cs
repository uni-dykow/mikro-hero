﻿namespace MikroHero
{
    public class SwordStrategy : IWeaponStrategy
    {

        public int Dmg { get; set; }
        public SwordStrategy(int dmg)
        {
            this.Dmg = dmg;
        }
        
        public int Use()
        {
            return Dmg;
        }
    }
}
