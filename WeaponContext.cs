﻿namespace MikroHero
{
    public static class WeaponContext
    {
        public static int UseWeapon(IWeaponStrategy weapon)
        {
            return weapon.Use();
        }
    }
}
