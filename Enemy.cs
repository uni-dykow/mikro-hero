using System;

namespace MikroHero
{
    public class Enemy : IHero
    {
        public int HealthPoints { get; set; } = 100;

        public int Damage()
        {
            var  dmg = WeaponContext.UseWeapon(new SwordStrategy(30));
            return dmg;
        }

        public bool IsAlive()
        {
            return HealthPoints > 0;
        }

        public int TakeDamage(int dmg)
        {
            HealthPoints -= dmg;
            return HealthPoints;
        }

        public void ResetHealthPoints()
        {
            HealthPoints = 100; 
        }
    }
}