﻿namespace MikroHero
{
    public class FireballStrategy : IWeaponStrategy
    {
        public int Dmg { get; set; }
        public FireballStrategy(int dmg)
        {
            this.Dmg = dmg;
        }
        
        public int Use()
        {
            return Dmg;
        }
    }
}